const path = require('path');
const webpack = require('webpack');
const html = require('html-webpack-plugin');
const copy = require('copy-webpack-plugin');

/*
 * SplitChunksPlugin is enabled by default and replaced
 * deprecated CommonsChunkPlugin. It automatically identifies modules which
 * should be splitted of chunk by heuristics using module duplication count and
 * module category (i. e. node_modules). And splits the chunks…
 *
 * It is safe to remove "splitChunks" from the generated configuration
 * and was added as an educational example.
 *
 * https://webpack.js.org/plugins/split-chunks-plugin/
 *
 */

/*
 * We've enabled TerserPlugin for you! This minifies your app
 * in order to load faster and run less javascript.
 *
 * https://github.com/webpack-contrib/terser-webpack-plugin
 *
 */

const TerserPlugin = require('terser-webpack-plugin');

var config = {
	mode: 'development',
	entry: {
		simple: path.resolve('src/simple.js'),
		multiple: path.resolve('src/multiple.js'),
		components: path.resolve('src/multiple-components.js'),
	},
	output: {
		filename: 'app/[name].bundle.js',
	},
	plugins: [
		new html({
			template: path.resolve('assets/index.html'),
			chunks: []
		}),
		new webpack.ProgressPlugin(),
		new copy({
			patterns: [
				{from: 'assets/simple.tsv'},
				{from: 'assets/multiple.tsv'}
			]
		})
	],

	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				include: [path.resolve(__dirname, 'src')],
				loader: 'babel-loader'
			},
			{
				test: /.(scss|css)$/,

				use: [
					{
						loader: 'style-loader'
					},
					{
						loader: 'css-loader',

						options: {
							sourceMap: true
						}
					},
					{
						loader: 'sass-loader',

						options: {
							sourceMap: true
						}
					}
				]
			}
		]
	},

	optimization: {
		minimizer: [new TerserPlugin()],

		splitChunks: {
			cacheGroups: {
				vendors: {
					priority: -10,
					test: /[\\/]node_modules[\\/]/
				}
			},

			chunks: 'async',
			minChunks: 1,
			minSize: 30000,
			name: false
		}
	}
};

Object.keys(config.entry).forEach( e => {
	config.plugins.push(new html({
		template: path.resolve('assets/template.html'),
		chunks: [e],
		filename: `${e}.html`
	}));
} );

module.exports = config;