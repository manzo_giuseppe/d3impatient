import * as d3 from 'd3'
import { geoGraticule10, schemeSet3, svg } from 'd3';


(function(){

    d3.tsv('multiple.tsv')
        .then((rows)=>{

            function makeScale( accessor, from, to ) {
                return d3.scaleLinear()
                    .domain( d3.extent(rows, accessor) )
                    .range([from,to]).nice();
            }

            const svg = d3.select('svg');

            //Set up max value for scales to a bit less of svg sizes
            const pX = svg.attr('width');
            const pY = svg.attr('height');

            //Set up  scale for x
            const xScale = makeScale( (r)=>r.x, 10, pX );

            //Set up scale for y1
            const y1Scale = makeScale( (r)=>r.y1, pY, 10 );

            //Set up scale for y1
            const y2Scale = makeScale( (r)=>r.y2, pY, 10 );

            //Draw function for draw points and lines
            function draw( g, accessor, curve ) {
                g.selectAll('circle').data(rows).enter()
                    .append('circle')
                        .attr('r', 5)
                        .attr('cx', (r)=>xScale(r.x) )
                        .attr('cy', accessor);
                
                const mkLine = d3.line().curve(curve)
                    .x( (r)=>xScale(r.x) )
                    .y( accessor );
                
                    g.append('path')
                        .attr('fill', 'none')
                        .attr('d', mkLine(rows));
            }

            const g1 = svg.append('g');
            const g2 = svg.append('g');

            draw( g1, (r)=>y1Scale(r.y1), d3.curveStep);
            draw( g2, (r)=>y2Scale(r.y2), d3.curveNatural);
            
            g1.selectAll('circle').attr('fill', 'green');
            g1.selectAll('path').attr('stroke', 'yellow');
            
            g2.selectAll('circle').attr('fill', 'blue');
            g2.selectAll('path').attr('stroke', 'red');

            var axMaker = d3.axisRight(y1Scale);
            axMaker( svg.append('g') );

            axMaker = d3.axisLeft( y2Scale );
            svg.append('g')
                .attr('transform', `translate(${pX},0)`)
                .call( axMaker );

            svg.append('g').call( d3.axisTop(xScale) )
                .attr('transform', `translate(0,${pY})`)
        });

})();
