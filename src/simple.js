import * as d3 from 'd3'


(function(){

    d3.tsv('simple.tsv')
        .then((rows)=>{

            d3.select('svg')
                .selectAll('circle')
                .data(rows)
                .enter()
                    .append('circle')
                        .attr('r', 5)
                        .attr('fill','red' )
                        .attr('cx', (d)=>d.x)
                        .attr('cy', (d)=>d.y)
                        

        });

})();
