import * as d3 from 'd3'
import { schemeSet3 } from 'd3';


(function(){

    d3.tsv('multiple.tsv')
        .then((rows)=>{

            //Set up max value for scales to a bit less of svg sizes
            const pY = 500 - 10;
            const pX = 900 - 10;

            //Set up  scale for x
            const xScale = d3.scaleLinear()
                .domain( d3.extent(rows, (r)=>r.x) )
                .range( [10, pX]);
            
            //Set up scale for y1
            const y1Scale = d3.scaleLinear()
                .domain( d3.extent( rows, (r)=>r.y1 ) )
                .range([pY, 10]);

            //Set up scale for y1
            const y2Scale = d3.scaleLinear()
                .domain( d3.extent( rows, (r)=>r.y2 ) )
                .range([pY, 10]);
                
            //Draw y1 datappoints
            d3.select('svg')
                .append('g').attr('id', 'ds1')
                .selectAll('circle')
                .data(rows).enter().append('circle')
                    .attr('r', '5').attr('fill', 'green')
                    .attr('cx', (r)=>xScale(r.x))
                    .attr('cy', (r)=>y1Scale(r.y1));

                
            //Draw x1 datappoints
            d3.select('svg')
                .append('g').attr('id', 'ds2').attr('fill', 'blue')
                .selectAll('circle')
                .data(rows).enter().append('circle')
                    .attr('r', '5')
                    .attr('cx', (r)=>xScale(r.x))
                    .attr('cy', (r)=>y2Scale(r.y2));

            //Draw lines for y1
            const lineMaker = d3.line()
                .x( (r)=>xScale(r.x) )
                .y( (r)=>y1Scale(r.y1) );
            
            d3.select('#ds1')
                .append('path')
                    .attr('fill', 'none').attr('stroke', 'red')
                    .attr('d', lineMaker(rows));
            
            //Draw lines for y2
            lineMaker.y( (r)=>y2Scale(r.y2) );
            
            d3.select('#ds2')
                .append('path')
                    .attr('fill', 'none').attr('stroke', 'green')
                    .attr('d', lineMaker(rows));
        });

})();
